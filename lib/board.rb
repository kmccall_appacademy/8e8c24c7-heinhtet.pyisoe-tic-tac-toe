class Board
  attr_accessor :board
  attr_accessor :board_graphics
  attr_accessor :grid
  def initialize(grid=[ [nil, nil, nil],
                        [nil, nil, nil],
                        [nil, nil, nil], ])
    @grid = grid
  end

  def board
   @board = board
  end

  def place_mark(pos, mark)
  @grid[pos[0]][pos[1]] = mark
  end

  def empty?(arr)
  return true if @grid[arr[0]][arr[1]] == nil
  false
  end

  def winner
    #horizontal combos
    if grid[0].all? {|el| el == :X}
      return :X
    elsif grid[1].all? {|el| el == :X}
      return :X
    elsif grid[2].all? {|el| el == :X}
      return :X
    elsif grid[0].all? {|el| el == :O}
      return :O
    elsif grid[1].all? {|el| el == :O}
      return :O
    elsif grid[2].all? {|el| el == :O}
      return :O
    end

    #vertical combos
    column0 = []
    column1 = []
    column2 = []
    grid.each do |column|
      column0 << column[0]
      column1 << column[1]
      column2 << column[2]
    end
    return :X if column0.all? {|el| el == :X}
    return :X if column1.all? {|el| el == :X}
    return :X if column2.all? {|el| el == :X}
     return :O if column0.all? {|el| el == :O}
     return :O if column1.all? {|el| el == :O}
     return :O if column2.all? {|el| el == :O}

    #diagonal combos
    if grid[1][1] == :X
        #diag l to r
        if grid[0][0] == :X && grid[2][2] == :X
          return :X
          #diag r to l
        elsif grid[0][2] == :X && grid[2][0] == :X
          return :X
        end
    end
    if grid[1][1] == :O
        #diag l to r
        if grid[0][0] == :O && grid[2][2] == :O
          return :O
          #diag r to l
        elsif grid[0][2] == :O && grid[2][0] == :O
          return :O
        end
    end

    nil
  end

  def over?
    grid.each do |array|
      return false if array.any? {|el| el == nil} && winner == nil
    end
    true
  #if grid array is all full of values & no combos found
  end

  def board_graphics
    puts " #{@grid[0][0]}   |  #{@grid[0][1]}  | #{@grid[0][2]} "
    puts "-----|-----|-----"
    puts " #{@grid[1][0]}   |  #{@grid[1][1]}  | #{@grid[1][2]} "
    puts "-----|-----|-----"
    puts " #{@grid[2][0]}   |  #{@grid[2][1]}  | #{@grid[2][2]} "
  end

  def horizontal_combo?(mark)
    return true if @grid[0].count(mark) > 1
    return true if @grid[1].count(mark) > 1
    return true if @grid[2].count(mark) > 1
    false
  end

  def vert_combo?(mark)
    column0 = []
    column1 = []
    column2 = []
    @grid.each do |column|
      column0 << column[0]
      column1 << column[1]
      column2 << column[2]
    end
    return true if column0.count(mark) == 2
    return true if column1.count(mark) == 2
    return true if column2.count(mark) == 2
  end

  def diag_combo?(mark)
    if grid[1][1] == mark
      if grid[0][0] == mark
        return true
      elsif grid[0][2] == mark
        return true
      elsif grid[2][0] == mark
        return true
      elsif grid[2][2] == mark
        return true
      end
    end
  end


end
