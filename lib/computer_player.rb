class ComputerPlayer
  attr_reader :name
  attr_accessor :mark
  attr_accessor :board

  def initialize(name)
    @name = name
    @mark = mark
  end

  def mark=(mark)
    @mark = mark
  end

  def get_move
    if board.horizontal_combo?(mark)
      row = 0
      column = 2
    elsif board.vert_combo?(mark)
      row = 2
      column = 0
    elsif board.diag_combo?(mark)
      row = 0
      column = 0
    else #no combo available
      row = rand(0..2)
      column = rand(0..2)
    end
    [row, column]
  end

  def display(board)
    @board = board
    puts board.board_graphics
  end


end
