require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board
  attr_accessor :board_graphics
  attr_reader   :current_player

  def initialize(player_one, player_two)
  @player_one = player_one
  @player_two = player_two
  @current_player = @player_one
  @board = Board.new
  end


  def switch_players!
      if @current_player == @player_one
        @current_player = @player_two
      else
        @player_one = @current_player
      end
  end

  def play_turn
    @current_player.display(@board)
    board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
  end

  def place_mark(move, token)
board[move1,move2] = token
  end

  def play
    until winner
      play_turn
    end
  end

end
