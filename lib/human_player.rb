class HumanPlayer
  attr_reader :name
  attr_accessor :mark
  attr_reader :board

  def initialize(name)
    @name = name
    @mark = mark
  end


  def display(board)
  @board = board
  puts board.board_graphics
  end

  def get_move
    puts "Put where? Use this format: row, column"
    puts "For example: 0,0 is the top left. 1,1 is the middle."
    move = gets.chomp
    move = move.split(",")
    [move[0].to_i, move[1].to_i]
  end



end
